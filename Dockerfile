FROM alpine:3.9

RUN apk add --no-cache \
  imagemagick \
  parallel

VOLUME /in
VOLUME /out

CMD find /in -type f -iname "*.jpg" | parallel convert {} -verbose -resize 480x480 -quality 75 /out/{/}
