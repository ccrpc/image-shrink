# Shrink Images in Parallel

# Usage
```
docker run --rm -v "./large:/in:ro" -v "./small:/out" registry.gitlab.com/ccrpc/image-shrink
```
